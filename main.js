const electron = require('electron');
const url = require('url');
const path = require('path');

const {
    app,
    BrowserWindow,
    Menu
} = electron;

// Set NODE_ENV
process.env.NODE_ENV = 'dev';

const menuTemplate = [
    {
        label: 'Edit',
        submenu: [
            {
                role: 'undo'
            },
            {
                role: 'redo'
            },
            {
                type: 'separator'
            },
            {
                role: 'cut'
            },
            {
                role: 'copy'
            },
            {
                role: 'paste'
            },
            {
                role: 'pasteandmatchstyle'
            },
            {
                role: 'delete'
            },
            {
                role: 'selectall'
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            // Dev features are added for non-prod mode
            {
                role: 'resetzoom'
            },
            {
                role: 'zoomin'
            },
            {
                role: 'zoomout'
            },
            {
                type: 'separator'
            },
            {
                role: 'togglefullscreen'
            }
        ]
    },
    {
        role: 'window',
        submenu: [
            {
                role: 'minimize'
            },
            {
                role: 'close'
            }
        ]
    },
    {
        role: 'help',
        submenu: [
            {
                label: 'Learn More',
                click() {
                    electron.shell.openExternal('https://gitlab.com/Otiose/OtioseDesktop');
                }
            }
        ]
    }
];

if (process.env.NODE_ENV === 'dev') {
    menuTemplate[1].submenu.unshift(
        {
            role: 'reload'
        },
        {
            role: 'forcereload'
        },
        {
            role: 'toggledevtools'
        },
        {
            type: 'separator'
        }
    );
}

if (process.platform === 'darwin') {
    menuTemplate.unshift({
        label: 'Otiose',
        submenu: [
            {
                role: 'about'
            },
            {
                type: 'separator'
            },
            {
                role: 'services',
                submenu: []
            },
            {
                type: 'separator'
            },
            {
                role: 'hide'
            },
            {
                role: 'hideothers'
            },
            {
                role: 'unhide'
            },
            {
                type: 'separator'
            },
            {
                role: 'quit'
            }
        ]
    });

    // Edit menu
    menuTemplate[1].submenu.push({
        type: 'separator'
    }, 
    {
        label: 'Speech',
        submenu: [
            {
                role: 'startspeaking'
            },
            {
                role: 'stopspeaking'
            }
        ]
    });

    // Window menu
    menuTemplate[3].submenu = [
        {
            role: 'close'
        },
        {
            role: 'minimize'
        },
        {
            role: 'zoom'
        },
        {
            type: 'separator'
        },
        {
            role: 'front'
        }
    ];
}

let mainWindow;
app.on('ready', function () {
    // Create Window
    mainWindow = new BrowserWindow({});
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Load Menu
    const menu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(menu);
});